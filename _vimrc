if has('gui_macvim')

		set antialias
		set showtabline=2    " タブを常に表示
		set transparency=0   " 透明度
		set imdisable        " IME OFF
		set guioptions-=T    " ツールバー非表示
		set antialias        " アンチエイリアス
		set tabstop=4        " タブサイズ
		set number           " 行番号表示
		set nobackup         " バックアップなし
		set noswapfile
		set visualbell t_vb= " ビープ音なし
		set columns=180      " 横幅
		set lines=60         " 行数
		set nowrapscan       " 検索をファイルの先頭へループしない
		set autoindent       " 新しい行のインデントを現在と同じにする
		set shiftwidth=4     " シフト移動幅
		set showmatch        " 閉じ括弧が入力されたとき、対応する括弧を表示する
		set smartcase        " 検索時に大文字を含んでいたら大/小を区別
		set laststatus=2
		set statusline=%<%f%=%h%w%y%{'['.(&fenc!=''?&fenc:&enc).']['.&ff.']'}%9(\ %m%r\ %)[%4v][%12(\ %5l/%5L%)]
		set encoding=utf-8
		set fileencodings=iso-2022-jp,euc-jp,sjis,utf-8
		set fileformats=unix,mac,dos
		set clipboard=unnamedplus,unnamed "ヤンクした文字は、システムのクリップボードに入れる

		filetype plugin on "ファイルタイプ判定をon
		set guifontwide=M+\ 1m\ light:h13 " フォント設定
		au! BufNewFile,BufRead *.as :set filetype=actionscript
		au BufNewFile,BufRead *.as set ft=actionscript
	
		noremap GG ggVG
		nnoremap gr gT
		noremap FF :Explore<CR>
		map <silent> <F2> :nohlsearch<CR>
		map <silent> <F1> :nohlsearch<CR>
		nnoremap ,v :<C-u>tabnew ~/.vimrc<CR>
		nnoremap NT :<C-u>tabnew<CR>
		nnoremap <F5> :<C-u>source ~/.vimrc<CR>
		nnoremap <SPACE>e :<C-u>Error<CR>
		noremap j gj
		noremap k gk
		nmap n nzz
		nmap N Nzz
		vnoremap v $h
		nnoremap gl :<C-u>sp<CR>:<C-u>VimShell<CR><CR><C-u>gulp<CR>
		
		" CTRL-hjklでウィンドウ移動
		nnoremap <C-j> <C-w>j
		nnoremap <C-k> <C-w>k
		nnoremap <C-l> <C-w>l
		nnoremap <C-h> <C-w>h
		" <Leader>キーを「,」に変更
		let mapleader = ","

		"日付挿入
		inoremap <Leader><Leader>date <C-R>=strftime('%Y/%m/%d (%a)')<CR>
		inoremap <Leader><Leader>time <C-R>=strftime('%H:%M')<CR>
		inoremap <Leader><Leader>w3cd <C-R>=strftime('%Y-%m-%dT%H:%M:%S+09:00')<CR>
		
		"Insertmode
		inoremap {} {}<LEFT>
		inoremap [] []<LEFT>
		inoremap () ()<LEFT>
		inoremap "" ""<LEFT>
		inoremap '' ''<LEFT>
		inoremap <> <><LEFT>
		
		set list
		set listchars=tab:»-,trail:-,eol:↲,extends:»,precedes:«,nbsp:%
		highlight SpecialKey term=underline ctermfg=darkgray guifg=#F5DEB3
		if has('multi_byte_ime') || has('xim')
 	   	highlight CursorIM guibg=White guifg=NONE "日本語入力時にカーソルの色を変更
		endif
	
		" 挿入モードでのカーソル移動
		" 下に移動
		inoremap <C-j> <Down>
		" 上に移動
		inoremap <C-k> <Up>
		" 左に移動
		inoremap <C-h> <Left>
		" 右に移動
		inoremap <C-l> <Right>

		"連続コピペ
		vnoremap <silent> <M-p> "0p<CR>

		"プラグイン
		set nocompatible
		if has('vim_starting')
			set runtimepath+=~/.vim/bundle/neobundle.vim
		endif
		call neobundle#rc(expand('~/.vim/.bundle'))

		" ここに:NeoBundleコマンドを列挙していく(後述)
		NeoBundle 'YankRing.vim'
		NeoBundle 'h1mesuke/vim-alignta'
		NeoBundle 'tpope/vim-surround'
		NeoBundle 'Shougo/neocomplcache'
		NeoBundle 'Shougo/neosnippet'
		NeoBundle 'Shougo/unite.vim'
		NeoBundle 'Shougo/vimshell'
		NeoBundle 'Shougo/vimproc'
		NeoBundle 'kannokanno/previm'
		NeoBundle 'Quramy/tsuquyomi'
		NeoBundle 'itchyny/lightline.vim'
		NeoBundle 'Lokaltog/vim-easymotion'
		NeoBundle 'tyru/open-browser.vim'
		NeoBundle 'tell-k/vim-browsereload-mac'
		NeoBundle 'hail2u/vim-css3-syntax'
		NeoBundle 'taichouchou2/html5.vim'
		NeoBundle 'taichouchou2/vim-javascript'
		NeoBundle 'thinca/vim-quickrun'
		NeoBundle 'osyo-manga/vim-over'
		NeoBundle 'scrooloose/syntastic'
		NeoBundle 'leafgarland/typescript-vim'
		"NeoBundle 'clausreinke/typescript-tools'

		filetype plugin indent on

		let g:previm_open_cmd = 'open -a Firefox'


		"------------------------------------------------------------------------------------
		" neoconmplcache
		"------------------------------------------------------------------------------------

		"" neocomplcache
		" Disable AutoComplPop.
		let g:acp_enableAtStartup = 0
		" Use neocomplcache.
		let g:neocomplcache_enable_at_startup = 1
		" Use smartcase.
		let g:neocomplcache_enable_smart_case = 1
		" Set minimum syntax keyword length.
		let g:neocomplcache_min_syntax_length = 3
		let g:neocomplcache_lock_buffer_name_pattern = '\*ku\*'

		" Define dictionary.
		let g:neocomplcache_dictionary_filetype_lists = {
			\ 'default' : ''
			\ }

		" Plugin key-mappings.
		inoremap <expr><C-g>     neocomplcache#undo_completion()
		inoremap <expr><C-l>     neocomplcache#complete_common_string()

		" Recommended key-mappings.
		" <CR>: close popup and save indent.
		inoremap <silent> <CR> <C-r>=<SID>my_cr_function()<CR>
		function! s:my_cr_function()
		  return neocomplcache#smart_close_popup() . "\<CR>"
		endfunction
		" <TAB>: completion.
		inoremap <expr><TAB>  pumvisible() ? "\<C-n>" : "\<TAB>"
		" <C-h>, <BS>: close popup and delete backword char.
		inoremap <expr><C-h> neocomplcache#smart_close_popup()."\<C-h>"
		inoremap <expr><BS> neocomplcache#smart_close_popup()."\<C-h>"
		inoremap <expr><C-y>  neocomplcache#close_popup()
		inoremap <expr><C-e>  neocomplcache#cancel_popup()

		"------------------------------------------------------------------------------------
	
		"------------------------------------------------------------------------------------
		" Align
		"------------------------------------------------------------------------------------
		let g:Align_xstrlen = 3


		" 横分割時は下へ､ 縦分割時は右へ新しいウィンドウが開くようにする
		set splitbelow
		set splitright
		
		" カレントディレクトリを自動的に移動
		au BufEnter * execute ":lcd " . expand("%:p:h")
		
		"選択部を検索
		vnoremap * "zy:let @/ = @z<CR>n


		"------------------------------------------------------------------------------------
		" EasyMotion
		"------------------------------------------------------------------------------------
		" ホームポジションに近いキーを使う
		let g:EasyMotion_keys='hjklasdfgyuiopqwertnmzxcvbHJKLASDFGYUIOPQWERTNMZXCVB'
		" 「;」 + 何かにマッピング
		let g:EasyMotion_leader_key=";"
		" 1 ストローク選択を優先する
		let g:EasyMotion_grouping=1
		" カラー設定変更
		hi EasyMotionTarget ctermbg=none ctermfg=red
		hi EasyMotionShade  ctermbg=none ctermfg=blue


		"------------------------------------------------------------------------------------
		" open browser
		"------------------------------------------------------------------------------------
		" カーソル下のURLをブラウザで開く
		nmap <Leader>o <Plug>(openbrowser-open)
		vmap <Leader>o <Plug>(openbrowser-open)
		" ググる
		nnoremap <Leader>g :<C-u>OpenBrowserSearch<Space><C-r><C-w><Enter>


		"------------------------------------------------------------------------------------
		" vim-browsereload-mac
		"------------------------------------------------------------------------------------
		
		" リロード後に戻ってくるアプリ 変更してください
		let g:returnApp = "MacVim"
		nmap <Space>bc :ChromeReloadStart<CR>
		nmap <Space>bC :ChromeReloadStop<CR>
		nmap <Space>bf :FirefoxReloadStart<CR>
		nmap <Space>bF :FirefoxReloadStop<CR>
		nmap <Space>bs :SafariReloadStart<CR>
		nmap <Space>bS :SafariReloadStop<CR>
		nmap <Space>bo :OperaReloadStart<CR>
		nmap <Space>bO :OperaReloadStop<CR>
		nmap <Space>ba :AllBrowserReloadStart<CR>
		nmap <Space>bA :AllBrowserReloadStop<CR>


		"------------------------------------------------------------------------------------
		" over.vim
		"------------------------------------------------------------------------------------
		""over.vim {{{

		"over.vimの起動
		nnoremap <silent> <Leader>m :OverCommandLine<CR>

		"カーソル下の単語をハイライト付きで置換
		nnoremap sub :OverCommandLine<CR>%s/<C-r><C-w>//g<Left><Left>

		"コピーした文字列をハイライト付きで置換
		nnoremap subp y:OverCommandLine<CR>%s!<C-r>=substitute(@0, '!', '\\!', 'g')<CR>!!gI<Left><Left><Left>

		"}}}
		
		" vモードの置換連続ペースト用
		function! Put_text_without_override_register()
		  let line_len = strlen(getline('.'))
		  execute "normal! `>"
		  let col_loc = col('.')
		  execute 'normal! gv"_x'
		  if line_len == col_loc
			execute 'normal! p'
		  else 
			execute 'normal! P'
		  endif
		endfunction
		xnoremap <silent> p :call Put_text_without_override_register()<CR>

		set clipboard=unnamed "ヤンクした時に自動でクリップボードにコピー(autoselectを指定するとvモードの置換連続ペーストができない)


endif
